# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [7.4.0](https://bitbucket.org/upassist/upassist-neos-frontendlogin/compare/5.1.1...7.4.0) (2022-10-05)
